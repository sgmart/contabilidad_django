from django import forms

from clasificador_objeto_gasto.models import (Capitulo, Concepto,
                                PartidaGenerica, PartidaEspecifica)

from django.db.models.signals import pre_save
from django.dispatch import receiver

VALUES = (
    (0, 0),
    (1, 1),
    (2, 2),
    (3, 3),
    (4, 4),
    (5, 5),
    (6, 6),
    (7, 7),
    (8, 8),
    (9, 9),
)

class CapituloForm(forms.ModelForm):
    clave = forms.ChoiceField(choices=VALUES)
    @receiver(pre_save, sender=Capitulo)
    def genera_clave(sender, instance, **kargs):
        capitulo_id = instance.clave
        instance.clave = instance.clave * 1000

    class Meta:
        model = Concepto


class ConceptoForm(forms.ModelForm):
    clave = forms.ChoiceField(choices=VALUES)
    @receiver(pre_save, sender=Concepto)
    def genera_clave(sender, instance, **kargs):
        cap = instance.capitulo.clave
        capitulo_id = instance.clave * 100
        instance.clave = cap + capitulo_id

    class Meta:
        model = Concepto


class PartidaGenericaForm(forms.ModelForm):
    clave = forms.ChoiceField(choices=VALUES)
    @receiver(pre_save, sender=PartidaGenerica)
    def genera_clave(sender, instance, **kargs):
        pre_concepto = Concepto.objects.get(pk=instance.concepto.id)
        pre_clave = pre_concepto.clave / 100

        instance.clave = int(str(pre_clave) + str(instance.clave))

    class Meta:
        model = PartidaGenerica


class PartidaEspecificaForm(forms.ModelForm):
    clave = forms.ChoiceField(choices=VALUES)
    @receiver(pre_save, sender=PartidaEspecifica)
    def genera_clave(sender, instance, **kargs):
        generica = PartidaGenerica.objects.get(pk=instance.partida_generica.id)
        pre_clave = generica.clave * 10

        partida_id = instance.clave
#        try:
#            obj = PartidaEspecifica.objects.get(clave=partida_id)
#        except PartidaGenerica.DoesNotExist:
        instance.clave = int(str(pre_clave) + str(partida_id))

    class Meta:
        model = PartidaEspecifica


class SearchForm(forms.Form):
    query = forms.CharField(
        label = 'Enter a keyword to search for',
        widget = forms.TextInput(attrs={'size': 32})
    )

