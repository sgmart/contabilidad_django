from django.contrib import admin
from django.contrib.auth.models import User, Group
from django.contrib.auth.admin import UserAdmin

class UserAdmin(UserAdmin):
    add_fieldsets = (
        (None, {
                'classes': ('wide',),
                'fields': ('username', 'email', 'password1',
                           'password2', 'is_superuser'),
            }
        ),
    )

admin.site.unregister(User)
admin.site.register(User, UserAdmin)
