import floppyforms
from django import forms


class SearchForm(floppyforms.Form):
    query = forms.CharField(
        label = 'Clave',
        widget = forms.TextInput(attrs={'size': 22})
    )

    fecha_i = floppyforms.DateField(
        label = 'Fecha inicial',
    )

    fecha_f = floppyforms.DateField(
        label = 'Fecha final',
    )
