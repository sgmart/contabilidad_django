from django import forms

from django.db.models.signals import pre_save
from django.dispatch import receiver

from cuentas_contables.models import *


# Clases ModelForm
class CuentaForm(forms.ModelForm):
    clave = forms.CharField(min_length=7, max_length=7)

    class Meta:
        model = Cuenta


class SubCuentaForm(forms.ModelForm):
    @receiver(pre_save, sender=SubCuenta)
    def generar_clave(sender, instance, **kargs):
        try:
            sub = SubCuenta.objects.get(clave=instance.clave)
        except SubCuenta.DoesNotExist:
            instance.clave = int(str(instance.cuenta.clave) +
                    str(instance.clave))

    class Meta:
        model = SubCuenta


class AsientoContableForm(forms.ModelForm):
    class Meta:
        model = AsientoContable

    @receiver(pre_save, sender=AsientoContable)
    def verificar_sumas(sender, instance, **kargs):
        resultado = AsientoContable.objects.verificar_consistencia()

#        if resultado:
#            Raise

