from django.db import models
from cuentas_contables.models import Entidad


class Capitulo(Entidad):
    class Meta:
        ordering = ('clave',)


class Concepto(Entidad):
    capitulo = models.ForeignKey(Capitulo)

    class Meta:
        ordering = ('clave',)


class PartidaGenerica(Entidad):
    concepto = models.ForeignKey(Concepto)

    class Meta:
        ordering = ('clave',)
        verbose_name = 'Partidas Genericas'


class PartidaEspecifica(Entidad):
    partida_generica = models.ForeignKey(PartidaGenerica)

    class Meta:
        ordering = ('clave',)
        unique_together = (('clave', 'nombre'),)
        verbose_name = 'Partidas Especificas'

