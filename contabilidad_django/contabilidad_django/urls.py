from django.conf.urls import patterns, include, url
from django.contrib.auth.models import Group

from django.contrib import admin
admin.autodiscover()

#admin.site.unregister(Group)
from django.contrib.auth.views import login

from base.views import hello_pdf
from contabilidad_django.views import *

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', pagina_inicial),
    url(r'^reporte-cuentas-contables/$', buscar_cc),
    url(r'^reporte-clasificador-objeto-gasto/$', buscar_cog),
    url(r'^login/$', login),
    url(r'^logout/$', logout_page),
)

