from django.shortcuts import render, render_to_response

from django.template import Context, RequestContext
from django.template.loader import get_template

from django.http import HttpResponse, Http404, HttpResponseRedirect

from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout

from contabilidad_django.forms import SearchForm

from clasificador_objeto_gasto.models import (Capitulo, Concepto,
        PartidaGenerica, PartidaEspecifica)
from cuentas_contables.models import *
from polizas.models import *
from django.contrib import messages

# Pagina inicial.
def pagina_inicial(request):
    return render_to_response(
        'index.html', {
			'user': request.user,
            'base_uri': '/',
            'titulo': 'DFSEP Guerrero - Sistema Contable',
            'contenido': "DFSEP Guerrero - Sistema Contable . . . . . . . . . ."
            + ". . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . "
            + ". . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . "
            + ". . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . "
            + ". . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . "
            + ". . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . "
            + ". . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . "
            + ". . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . "
            + ". . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . "
    })


def logout_page(request):
    logout(request)
    return HttpResponseRedirect('/')


@login_required(login_url='/login/')
def buscar_cc(request):
    '''
    Buscar beneficiario pertenecientes a las cuentas contables.
    '''
    form = SearchForm()
    beneficiario = []
    super_lista = []
    beneficiario_polizas = []

    mostrar_results = False
    if (request.GET.has_key('query')
            and request.GET.has_key('fecha_i')
            and request.GET.has_key('fecha_f')):
        query = request.GET['query'].strip()
        f1 = request.GET['fecha_i'].strip()
        f2 = request.GET['fecha_f'].strip()
        if query and f1 and f2:
            form = SearchForm({'query': query, 'fecha_i': f1, 'fecha_f': f2})
            if not beneficiario:
                beneficiario = Cuenta.objects.filter(clave=query)
                if not beneficiario:
                    beneficiario = SubCuenta.objects.filter(clave=query)

            if beneficiario:
                mostrar_results = True

                beneficiario_polizas = PolizaCheque.objects.filter(
                                            beneficiario=beneficiario,
                                            fecha__range=(f1, f2))
                poliza_tmp = []
                for poliza in beneficiario_polizas:
                    debe = AsientoDebeDetalle.objects.get(
                            asiento=poliza.asiento)
                    haber = AsientoHaberDetalle.objects.get(
                            asiento=poliza.asiento)
                    poliza_tmp.append(poliza.fecha)
                    poliza_tmp.append(poliza.numero_poliza)
                    poliza_tmp.append(poliza.beneficiario.clave)
                    poliza_tmp.append(poliza.beneficiario.nombre)
                    poliza_tmp.append(debe.monto)
                    poliza_tmp.append(haber.monto)
                    super_lista.append(poliza_tmp)
                    poliza_tmp = []


    variables = RequestContext(request, {'form': form,
        'titulo': 'Filtrar beneficiario desde el catalogo de cuentas contables',
        'entidades': super_lista,
        'mostrar_results': mostrar_results,
        'mostrar_tags': True,
        'mostrar_user': True
    })

    return render_to_response('buscar.html', variables)


@login_required(login_url='/login/')
def buscar_cog(request):
    '''
    Buscar beneficiario pertenecientes al clasificador por objeto del gasto.
    '''
    form = SearchForm()
    beneficiario = []
    super_lista = []

    mostrar_results = False
    if request.GET.has_key('query'):
        query = request.GET['query'].strip()
        if query:
            form = SearchForm({'query': query})
            mostrar_results = True
            if not beneficiario:
                beneficiario = Capitulo.objects.filter(clave=query)
                if not beneficiario:
                    beneficiario = Concepto.objects.filter(clave=query)
                    if not beneficiario:
                        beneficiario = PartidaEspecifica.objects.filter(clave=query)
                        if not beneficiario:
                            beneficiario = PartidaEspecifica.objects.filter(clave=query)

            if beneficiario:
                mostrar_results = True

                beneficiario_polizas = PolizaCheque.objects.filter(
                                            beneficiario=beneficiario,
                                            fecha__range=(f1, f2))
                poliza_tmp = []
                for poliza in beneficiario_polizas:
                    debe = AsientoDebeDetalle.objects.get(
                            asiento=poliza.asiento)
                    haber = AsientoHaberDetalle.objects.get(
                            asiento=poliza.asiento)
                    poliza_tmp.append(poliza.fecha)
                    poliza_tmp.append(poliza.numero_poliza)
                    poliza_tmp.append(poliza.beneficiario.clave)
                    poliza_tmp.append(poliza.beneficiario.nombre)
                    poliza_tmp.append(debe.monto)
                    poliza_tmp.append(haber.monto)
                    super_lista.append(poliza_tmp)
                    poliza_tmp = []

    variables = RequestContext(request, {'form': form,
        'titulo': 'Filtrar beneficiario desde el Clasificador por Objeto del Gasto',
        'entidades': super_lista,
        'mostrar_results': mostrar_results,
        'mostrar_tags': True,
        'mostrar_user': True
    })

    return render_to_response('buscar.html', variables)

