from django.contrib import admin

from cuentas_contables.models import *
from cuentas_contables.forms import *

class SubCuentaEnLinea(admin.TabularInline):
    model = SubCuenta
    extra = 1


class AsientoHaberDetalleEnlinea(admin.TabularInline):
    model = AsientoHaberDetalle
    extra = 1


class AsientoDebeDetalleEnlinea(admin.TabularInline):
    model = AsientoDebeDetalle
    extra = 1


# Clases ModelAdmin
class CuentaAdmin(admin.ModelAdmin):
    form = CuentaForm
    inlines = [SubCuentaEnLinea]
    list_display = ('clave', 'nombre',)


class SubCuentaAdmin(admin.ModelAdmin):
    form = SubCuentaForm
    list_display = ('clave', 'nombre')
    search_fields = ('clave', 'nombre')

class AsientoContableAdmin(admin.ModelAdmin):
    form = AsientoContableForm
    inlines = [AsientoHaberDetalleEnlinea, AsientoDebeDetalleEnlinea]
    list_display = ('id', 'comentario',)

    class Meta:
        model = AsientoContable


# Registar modelos
admin.site.register(AsientoContable, AsientoContableAdmin)
admin.site.register(Cuenta, CuentaAdmin)
admin.site.register(SubCuenta, SubCuentaAdmin)

