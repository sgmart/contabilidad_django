# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'PolizaCheque'
        db.create_table(u'polizas_polizacheque', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('numero_poliza', self.gf('django.db.models.fields.CharField')(unique=True, max_length=20)),
            ('concepto', self.gf('django.db.models.fields.TextField')()),
            ('fecha', self.gf('django.db.models.fields.DateField')()),
            ('asiento', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['cuentas_contables.AsientoContable'], unique=True)),
            ('elaboro', self.gf('django.db.models.fields.CharField')(max_length=65)),
            ('reviso', self.gf('django.db.models.fields.CharField')(max_length=65)),
            ('autorizo', self.gf('django.db.models.fields.CharField')(max_length=65)),
            ('beneficiario', self.gf('django.db.models.fields.related.ForeignKey')(related_name='ben_involucrado', to=orm['cuentas_contables.SubCuenta'])),
            ('importe_numero', self.gf('django.db.models.fields.DecimalField')(max_digits=19, decimal_places=2)),
            ('importe_letra', self.gf('django.db.models.fields.CharField')(max_length=250)),
            ('banco', self.gf('django.db.models.fields.related.ForeignKey')(related_name='banco_involucrado', to=orm['cuentas_contables.SubCuenta'])),
            ('folio_cheque', self.gf('django.db.models.fields.CharField')(unique=True, max_length=20)),
        ))
        db.send_create_signal(u'polizas', ['PolizaCheque'])

        # Adding model 'PolizaIngreso'
        db.create_table(u'polizas_polizaingreso', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('numero_poliza', self.gf('django.db.models.fields.CharField')(unique=True, max_length=20)),
            ('concepto', self.gf('django.db.models.fields.TextField')()),
            ('fecha', self.gf('django.db.models.fields.DateField')()),
            ('asiento', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['cuentas_contables.AsientoContable'], unique=True)),
            ('elaboro', self.gf('django.db.models.fields.CharField')(max_length=65)),
            ('reviso', self.gf('django.db.models.fields.CharField')(max_length=65)),
            ('autorizo', self.gf('django.db.models.fields.CharField')(max_length=65)),
        ))
        db.send_create_signal(u'polizas', ['PolizaIngreso'])

        # Adding unique constraint on 'PolizaIngreso', fields ['numero_poliza', 'asiento']
        db.create_unique(u'polizas_polizaingreso', ['numero_poliza', 'asiento_id'])

        # Adding model 'PolizaDiario'
        db.create_table(u'polizas_polizadiario', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('numero_poliza', self.gf('django.db.models.fields.CharField')(unique=True, max_length=20)),
            ('concepto', self.gf('django.db.models.fields.TextField')()),
            ('fecha', self.gf('django.db.models.fields.DateField')()),
            ('asiento', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['cuentas_contables.AsientoContable'], unique=True)),
            ('elaboro', self.gf('django.db.models.fields.CharField')(max_length=65)),
            ('reviso', self.gf('django.db.models.fields.CharField')(max_length=65)),
            ('autorizo', self.gf('django.db.models.fields.CharField')(max_length=65)),
        ))
        db.send_create_signal(u'polizas', ['PolizaDiario'])

        # Adding unique constraint on 'PolizaDiario', fields ['numero_poliza', 'asiento']
        db.create_unique(u'polizas_polizadiario', ['numero_poliza', 'asiento_id'])


    def backwards(self, orm):
        # Removing unique constraint on 'PolizaDiario', fields ['numero_poliza', 'asiento']
        db.delete_unique(u'polizas_polizadiario', ['numero_poliza', 'asiento_id'])

        # Removing unique constraint on 'PolizaIngreso', fields ['numero_poliza', 'asiento']
        db.delete_unique(u'polizas_polizaingreso', ['numero_poliza', 'asiento_id'])

        # Deleting model 'PolizaCheque'
        db.delete_table(u'polizas_polizacheque')

        # Deleting model 'PolizaIngreso'
        db.delete_table(u'polizas_polizaingreso')

        # Deleting model 'PolizaDiario'
        db.delete_table(u'polizas_polizadiario')


    models = {
        u'cuentas_contables.asientocontable': {
            'Meta': {'ordering': "['id']", 'object_name': 'AsientoContable'},
            'comentario': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'fecha': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'cuentas_contables.cuenta': {
            'Meta': {'object_name': 'Cuenta'},
            'clave': ('django.db.models.fields.IntegerField', [], {}),
            'descripcion': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '200'}),
            'tipo': ('django.db.models.fields.IntegerField', [], {'blank': 'True'})
        },
        u'cuentas_contables.subcuenta': {
            'Meta': {'ordering': "('clave',)", 'object_name': 'SubCuenta'},
            'clave': ('django.db.models.fields.IntegerField', [], {}),
            'cuenta': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cuentas_contables.Cuenta']"}),
            'descripcion': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '200'})
        },
        u'polizas.polizacheque': {
            'Meta': {'object_name': 'PolizaCheque'},
            'asiento': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['cuentas_contables.AsientoContable']", 'unique': 'True'}),
            'autorizo': ('django.db.models.fields.CharField', [], {'max_length': '65'}),
            'banco': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'banco_involucrado'", 'to': u"orm['cuentas_contables.SubCuenta']"}),
            'beneficiario': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'ben_involucrado'", 'to': u"orm['cuentas_contables.SubCuenta']"}),
            'concepto': ('django.db.models.fields.TextField', [], {}),
            'elaboro': ('django.db.models.fields.CharField', [], {'max_length': '65'}),
            'fecha': ('django.db.models.fields.DateField', [], {}),
            'folio_cheque': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'importe_letra': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'importe_numero': ('django.db.models.fields.DecimalField', [], {'max_digits': '19', 'decimal_places': '2'}),
            'numero_poliza': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '20'}),
            'reviso': ('django.db.models.fields.CharField', [], {'max_length': '65'})
        },
        u'polizas.polizadiario': {
            'Meta': {'unique_together': "(('numero_poliza', 'asiento'),)", 'object_name': 'PolizaDiario'},
            'asiento': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['cuentas_contables.AsientoContable']", 'unique': 'True'}),
            'autorizo': ('django.db.models.fields.CharField', [], {'max_length': '65'}),
            'concepto': ('django.db.models.fields.TextField', [], {}),
            'elaboro': ('django.db.models.fields.CharField', [], {'max_length': '65'}),
            'fecha': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'numero_poliza': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '20'}),
            'reviso': ('django.db.models.fields.CharField', [], {'max_length': '65'})
        },
        u'polizas.polizaingreso': {
            'Meta': {'unique_together': "(('numero_poliza', 'asiento'),)", 'object_name': 'PolizaIngreso'},
            'asiento': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['cuentas_contables.AsientoContable']", 'unique': 'True'}),
            'autorizo': ('django.db.models.fields.CharField', [], {'max_length': '65'}),
            'concepto': ('django.db.models.fields.TextField', [], {}),
            'elaboro': ('django.db.models.fields.CharField', [], {'max_length': '65'}),
            'fecha': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'numero_poliza': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '20'}),
            'reviso': ('django.db.models.fields.CharField', [], {'max_length': '65'})
        }
    }

    complete_apps = ['polizas']