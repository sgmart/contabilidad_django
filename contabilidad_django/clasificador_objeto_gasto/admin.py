#-*- coding:utf-8 -*-
from django.contrib import admin

from clasificador_objeto_gasto.models import (Capitulo, Concepto,
                                PartidaGenerica, PartidaEspecifica)

from clasificador_objeto_gasto.forms import (CapituloForm, ConceptoForm,
                                PartidaGenericaForm, PartidaEspecificaForm)

class PartidaGenericaEnLinea(admin.TabularInline):
    form = PartidaGenericaForm
    model = PartidaGenerica
    extra = 0


class PartidaEspecificaEnLinea(admin.TabularInline):
    form = PartidaGenericaForm
    model = PartidaEspecifica
    extra = 0


class ConceptoEnLinea(admin.TabularInline):
    form = ConceptoForm
    model = Concepto
    extra = 0


# Admin classes
class CapituloAdmin(admin.ModelAdmin):
    form = CapituloForm
    list_display = ('Clave', 'nombre', 'descripcion')

    inlines = [ConceptoEnLinea]


class PartidaGenericaAdmin(admin.ModelAdmin):
    form = PartidaGenericaForm
    fields = (('concepto', 'clave', 'nombre'), 'descripcion',)
    list_display = ('Clave', 'nombre', 'concepto')

    inlines = [PartidaEspecificaEnLinea]

    list_filter = ('concepto',)


class PartidaEspecificaAdmin(admin.ModelAdmin):
    form = PartidaEspecificaForm
    fields = ('partida_generica', ('clave', 'nombre', 'descripcion'),)
    list_display = ('Clave', 'nombre', 'partida_generica')

    list_filter = ('partida_generica',)


class ConceptoAdmin(admin.ModelAdmin):
    form = ConceptoForm
    fields = ('capitulo', ('clave', 'nombre', 'descripcion'),)
    list_display = ('Clave', 'nombre', 'capitulo')
    inlines = [PartidaGenericaEnLinea]
    list_filter = ('capitulo',)


# Registrar modelos en sitio de administración
admin.site.register(PartidaGenerica, PartidaGenericaAdmin)
admin.site.register(PartidaEspecifica, PartidaEspecificaAdmin)
admin.site.register(Concepto, ConceptoAdmin)
admin.site.register(Capitulo, CapituloAdmin)

