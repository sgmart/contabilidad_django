from django import forms

from django.dispatch import receiver
from django.db.models.signals import pre_save
from django.core.validators import MinValueValidator

from datetime import date

from num2words import num2words

from polizas.models import PolizaCheque, PolizaIngreso, PolizaDiario


def importe2letra(importe):
    importe_entero = int(importe) # Solo la parte entera

    parte_decimal = (importe - importe_entero) * 100
    centavos = int(parte_decimal)

    pre_importe_letra = num2words(importe_entero, lang='es')

    aux = pre_importe_letra.split()

    if aux[-1] == 'uno':
        pre_importe_letra = pre_importe_letra[:-1]

    if aux[0] == 'mil':
        pre_importe_letra = 'un ' + pre_importe_letra

    # setting importe_letra
    return ('(' +  pre_importe_letra + ' pesos ' +
                str(centavos) + '/100 M.N.)')


# ModelForms
class PolizaChequeForm(forms.ModelForm):
    importe_numero = forms.DecimalField(validators=[MinValueValidator(0.50)])

    class Meta:
        model = PolizaCheque
        exclude = ('importe_letra', 'numero_poliza',)

    @receiver(pre_save, sender=PolizaCheque)
    def generar_numero_poliza(sender, instance, **kargs):
        # Getting words from importe
        instance.importe_letra = importe2letra(instance.importe_numero)

        fecha = date.today()

        mes = fecha.month
        anio = fecha.year
        try:
            ultima_poliza = PolizaCheque.objects.last()
            ultimo_mes = int(ultima_poliza.numero_poliza.split('-')[2])
            ultimo_numero = int(ultima_poliza.numero_poliza.split('-')[1])

            if mes != ultimo_mes:
                instance.numero_poliza = ("E-" + str(1) + "-" + str(mes) +
                        "-" + str(anio))
            else:
                nuevo = ultimo_numero + 1
                instance.numero_poliza = ("E-" + str(nuevo) + "-" + str(mes)
                                          + "-" + str(anio))
        except:
            instance.numero_poliza = ("E-" + str(1) + "-" + str(mes) + "-"
                    + str(anio))


class PolizaIngresoForm(forms.ModelForm):
    class Meta:
        model = PolizaIngreso
        exclude = ('numero_poliza',)

    @receiver(pre_save, sender=PolizaIngreso)
    def generar_numero_poliza(sender, instance, **kargs):
        fecha = date.today()

        mes = fecha.month
        anio = fecha.year
        try:
            ultima_poliza = PolizaIngreso.objects.last()
            ultimo_mes = int(ultima_poliza.numero_poliza.split('-')[2])
            ultimo_numero = int(ultima_poliza.numero_poliza.split('-')[1])

            if mes != ultimo_mes:
                instance.numero_poliza = ("I-" + str(1) + "-" + str(mes) +
                        "-" + str(anio))
            else:
                nuevo = ultimo_numero + 1
                instance.numero_poliza = ("I-" + str(nuevo) + "-" + str(mes)
                                          + "-" + str(anio))
        except:
            instance.numero_poliza = ("I-" + str(1) + "-" + str(mes) + "-"
                    + str(anio))


class PolizaDiarioForm(forms.ModelForm):
    class Meta:
        model = PolizaDiario
        exclude = ('numero_poliza',)

    @receiver(pre_save, sender=PolizaDiario)
    def generar_numero_poliza(sender, instance, **kargs):
        fecha = date.today()

        mes = fecha.month
        anio = fecha.year
        try:
            ultima_poliza = PolizaDiario.objects.last()
            ultimo_mes = int(ultima_poliza.numero_poliza.split('-')[2])
            ultimo_numero = int(ultima_poliza.numero_poliza.split('-')[1])

            if mes != ultimo_mes:
                instance.numero_poliza = ("D-" + str(1) + "-" + str(mes) +
                        "-" + str(anio))
            else:
                nuevo = ultimo_numero + 1
                instance.numero_poliza = ("D-" + str(nuevo) + "-" + str(mes)
                                          + "-" + str(anio))
        except:
            instance.numero_poliza = ("D-" + str(1) + "-" + str(mes) + "-"
                    + str(anio))


