from django.contrib import admin

from polizas.models import PolizaCheque, PolizaIngreso, PolizaDiario
from polizas.forms import (PolizaChequeForm, PolizaIngresoForm,
        PolizaDiarioForm)

# Clases Admin
class PolizaChequeAdmin(admin.ModelAdmin):
    class Meta:
        model = PolizaCheque

#    raw_id_fields = ('beneficiario',) # TODO: Use it
    list_display = ('numero_poliza', 'fecha', 'beneficiario', 'folio_cheque')
    fields = ('beneficiario', 'fecha', 'importe_numero',
            ('banco', 'folio_cheque'), 'concepto', 'asiento',
            ('elaboro', 'reviso', 'autorizo'))
    form = PolizaChequeForm


class PolizaIngresoAdmin(admin.ModelAdmin):
    class Meta:
        model = PolizaIngreso

    list_display = ('numero_poliza', 'reviso', 'autorizo')
    fields = ('fecha', 'concepto', 'asiento', ('elaboro', 'reviso', 'autorizo'))

    form = PolizaIngresoForm


class PolizaDiarioAdmin(admin.ModelAdmin):
    class Meta:
        model = PolizaDiario

    list_display = ('numero_poliza', 'reviso', 'autorizo')
    fields = ('fecha', 'concepto', 'asiento', ('elaboro', 'reviso', 'autorizo'))

    form = PolizaDiarioForm


# Agrega modelos al sitio
admin.site.register(PolizaCheque, PolizaChequeAdmin)
admin.site.register(PolizaIngreso, PolizaIngresoAdmin)
admin.site.register(PolizaDiario, PolizaDiarioAdmin)

