from time import strptime, strftime
from django import forms
from django.db import models
from django.forms import fields
from contabilidad_django.widgets import JqSplitDateTimeWidget

class JqSplitDateTimeField(fields.MultiValueField):
    widget = JqSplitDateTimeWidget

    def __init__(self, *args, **kwargs):
        """
        Have to pass a list of field types to the constructor, else we
        won't get any data to our compress method.
        """
        all_fields = (
            fields.CharField(max_length=10),
            )
        super(JqSplitDateTimeField, self).__init__(all_fields, *args, **kwargs)
    
    def compress(self, data_list):
        """
        Takes the values from the MultiWidget and passes them as a
        list to this function. This function needs to compress the
        list into a single object to save.
        """
        if data_list:
            if not (data_list[0]):
                raise forms.ValidationError("Field is missing data.")

            print "Datetime: %s"% "%s %s" % (data_list[0])
            return data_list[0]
        return None
