# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Cuenta'
        db.create_table(u'cuentas_contables_cuenta', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('clave', self.gf('django.db.models.fields.IntegerField')()),
            ('nombre', self.gf('django.db.models.fields.CharField')(unique=True, max_length=200)),
            ('descripcion', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('tipo', self.gf('django.db.models.fields.IntegerField')(blank=True)),
        ))
        db.send_create_signal(u'cuentas_contables', ['Cuenta'])

        # Adding model 'SubCuenta'
        db.create_table(u'cuentas_contables_subcuenta', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('clave', self.gf('django.db.models.fields.IntegerField')()),
            ('nombre', self.gf('django.db.models.fields.CharField')(unique=True, max_length=200)),
            ('descripcion', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('cuenta', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cuentas_contables.Cuenta'])),
        ))
        db.send_create_signal(u'cuentas_contables', ['SubCuenta'])

        # Adding model 'AsientoContable'
        db.create_table(u'cuentas_contables_asientocontable', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('comentario', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('fecha', self.gf('django.db.models.fields.DateField')()),
        ))
        db.send_create_signal(u'cuentas_contables', ['AsientoContable'])

        # Adding model 'AsientoDebeDetalle'
        db.create_table(u'cuentas_contables_asientodebedetalle', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('asiento', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cuentas_contables.AsientoContable'])),
            ('cuenta', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cuentas_contables.SubCuenta'])),
            ('monto', self.gf('django.db.models.fields.DecimalField')(max_digits=19, decimal_places=2)),
        ))
        db.send_create_signal(u'cuentas_contables', ['AsientoDebeDetalle'])

        # Adding model 'AsientoHaberDetalle'
        db.create_table(u'cuentas_contables_asientohaberdetalle', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('asiento', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cuentas_contables.AsientoContable'])),
            ('cuenta', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cuentas_contables.SubCuenta'])),
            ('monto', self.gf('django.db.models.fields.DecimalField')(max_digits=19, decimal_places=2)),
        ))
        db.send_create_signal(u'cuentas_contables', ['AsientoHaberDetalle'])


    def backwards(self, orm):
        # Deleting model 'Cuenta'
        db.delete_table(u'cuentas_contables_cuenta')

        # Deleting model 'SubCuenta'
        db.delete_table(u'cuentas_contables_subcuenta')

        # Deleting model 'AsientoContable'
        db.delete_table(u'cuentas_contables_asientocontable')

        # Deleting model 'AsientoDebeDetalle'
        db.delete_table(u'cuentas_contables_asientodebedetalle')

        # Deleting model 'AsientoHaberDetalle'
        db.delete_table(u'cuentas_contables_asientohaberdetalle')


    models = {
        u'cuentas_contables.asientocontable': {
            'Meta': {'ordering': "['id']", 'object_name': 'AsientoContable'},
            'comentario': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'fecha': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'cuentas_contables.asientodebedetalle': {
            'Meta': {'object_name': 'AsientoDebeDetalle'},
            'asiento': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cuentas_contables.AsientoContable']"}),
            'cuenta': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cuentas_contables.SubCuenta']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'monto': ('django.db.models.fields.DecimalField', [], {'max_digits': '19', 'decimal_places': '2'})
        },
        u'cuentas_contables.asientohaberdetalle': {
            'Meta': {'object_name': 'AsientoHaberDetalle'},
            'asiento': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cuentas_contables.AsientoContable']"}),
            'cuenta': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cuentas_contables.SubCuenta']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'monto': ('django.db.models.fields.DecimalField', [], {'max_digits': '19', 'decimal_places': '2'})
        },
        u'cuentas_contables.cuenta': {
            'Meta': {'object_name': 'Cuenta'},
            'clave': ('django.db.models.fields.IntegerField', [], {}),
            'descripcion': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '200'}),
            'tipo': ('django.db.models.fields.IntegerField', [], {'blank': 'True'})
        },
        u'cuentas_contables.subcuenta': {
            'Meta': {'ordering': "('clave',)", 'object_name': 'SubCuenta'},
            'clave': ('django.db.models.fields.IntegerField', [], {}),
            'cuenta': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cuentas_contables.Cuenta']"}),
            'descripcion': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '200'})
        }
    }

    complete_apps = ['cuentas_contables']