# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'AsientoContable'
        db.create_table(u'base_asientocontable', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('comentario', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal(u'base', ['AsientoContable'])


    def backwards(self, orm):
        # Deleting model 'AsientoContable'
        db.delete_table(u'base_asientocontable')


    models = {
        u'base.asientocontable': {
            'Meta': {'object_name': 'AsientoContable'},
            'comentario': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['base']