# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Capitulo'
        db.create_table(u'clasificador_objeto_gasto_capitulo', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('clave', self.gf('django.db.models.fields.IntegerField')()),
            ('nombre', self.gf('django.db.models.fields.CharField')(unique=True, max_length=200)),
            ('descripcion', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal(u'clasificador_objeto_gasto', ['Capitulo'])

        # Adding model 'Concepto'
        db.create_table(u'clasificador_objeto_gasto_concepto', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('clave', self.gf('django.db.models.fields.IntegerField')()),
            ('nombre', self.gf('django.db.models.fields.CharField')(unique=True, max_length=200)),
            ('descripcion', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('capitulo', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['clasificador_objeto_gasto.Capitulo'])),
        ))
        db.send_create_signal(u'clasificador_objeto_gasto', ['Concepto'])

        # Adding model 'PartidaGenerica'
        db.create_table(u'clasificador_objeto_gasto_partidagenerica', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('clave', self.gf('django.db.models.fields.IntegerField')()),
            ('nombre', self.gf('django.db.models.fields.CharField')(unique=True, max_length=200)),
            ('descripcion', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('concepto', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['clasificador_objeto_gasto.Concepto'])),
        ))
        db.send_create_signal(u'clasificador_objeto_gasto', ['PartidaGenerica'])

        # Adding model 'PartidaEspecifica'
        db.create_table(u'clasificador_objeto_gasto_partidaespecifica', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('clave', self.gf('django.db.models.fields.IntegerField')()),
            ('nombre', self.gf('django.db.models.fields.CharField')(unique=True, max_length=200)),
            ('descripcion', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('partida_generica', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['clasificador_objeto_gasto.PartidaGenerica'])),
        ))
        db.send_create_signal(u'clasificador_objeto_gasto', ['PartidaEspecifica'])

        # Adding unique constraint on 'PartidaEspecifica', fields ['clave', 'nombre']
        db.create_unique(u'clasificador_objeto_gasto_partidaespecifica', ['clave', 'nombre'])


    def backwards(self, orm):
        # Removing unique constraint on 'PartidaEspecifica', fields ['clave', 'nombre']
        db.delete_unique(u'clasificador_objeto_gasto_partidaespecifica', ['clave', 'nombre'])

        # Deleting model 'Capitulo'
        db.delete_table(u'clasificador_objeto_gasto_capitulo')

        # Deleting model 'Concepto'
        db.delete_table(u'clasificador_objeto_gasto_concepto')

        # Deleting model 'PartidaGenerica'
        db.delete_table(u'clasificador_objeto_gasto_partidagenerica')

        # Deleting model 'PartidaEspecifica'
        db.delete_table(u'clasificador_objeto_gasto_partidaespecifica')


    models = {
        u'clasificador_objeto_gasto.capitulo': {
            'Meta': {'ordering': "('clave',)", 'object_name': 'Capitulo'},
            'clave': ('django.db.models.fields.IntegerField', [], {}),
            'descripcion': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '200'})
        },
        u'clasificador_objeto_gasto.concepto': {
            'Meta': {'ordering': "('clave',)", 'object_name': 'Concepto'},
            'capitulo': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['clasificador_objeto_gasto.Capitulo']"}),
            'clave': ('django.db.models.fields.IntegerField', [], {}),
            'descripcion': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '200'})
        },
        u'clasificador_objeto_gasto.partidaespecifica': {
            'Meta': {'ordering': "('clave',)", 'unique_together': "(('clave', 'nombre'),)", 'object_name': 'PartidaEspecifica'},
            'clave': ('django.db.models.fields.IntegerField', [], {}),
            'descripcion': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '200'}),
            'partida_generica': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['clasificador_objeto_gasto.PartidaGenerica']"})
        },
        u'clasificador_objeto_gasto.partidagenerica': {
            'Meta': {'ordering': "('clave',)", 'object_name': 'PartidaGenerica'},
            'clave': ('django.db.models.fields.IntegerField', [], {}),
            'concepto': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['clasificador_objeto_gasto.Concepto']"}),
            'descripcion': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '200'})
        }
    }

    complete_apps = ['clasificador_objeto_gasto']