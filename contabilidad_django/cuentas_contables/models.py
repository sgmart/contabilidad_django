from django.db import models

from django.db.models import Sum

TIPO_CUENTA = (
    (1, 'Acreedora'),
    (-1,  'Deudora'),
)

class Entidad(models.Model):
    clave = models.IntegerField()
    nombre = models.CharField(max_length=200, unique=True)
    descripcion = models.TextField(blank=True)

    def Clave(self):
        return str(self.clave)

    class Meta:
        abstract = True

    def __str__(self):
        return str(self.clave) + ' ' + self.nombre


class Cuenta(Entidad):
    tipo = models.IntegerField(choices=TIPO_CUENTA, blank=True)

    def __str__(self):
        return str(self.clave) + " " + self.nombre


class SubCuenta(Entidad):
    cuenta = models.ForeignKey(Cuenta)

    class Meta:
        ordering = ('clave',)


class AsientoContableManager(models.Manager):
    '''Manager personalizado para trabajar sobre todos los registros de
    Asientos'''
    def verificar_consistencia(self):
        '''Verifica si el debe y el haber tienen el mismo monto, para que la
        partida doble funcione.
        Devuelve la lista de asientos que estan mal'''
        resultado = []
        lista_de_asientos = self.all()
        for asiento in lista_de_asientos:
            lista_monto_debes = AsientoDebeDetalle.objects.filter(
                    asiento=asiento).aggregate(suma=Sum('monto'))
            lista_monto_haberes = AsientoHaberDetalle.objects.filter(
                    asiento=asiento).aggregate(suma=Sum('monto'))

            if lista_monto_debes['suma'] != lista_monto_haberes['suma']:
                resultado.append(asiento)

        return resultado


class AsientoContable(models.Model):
    comentario = models.TextField(blank=True)
    fecha = models.DateField()

    objects = AsientoContableManager()

    def __str__(self):
        return self.comentario

    class Meta:
        ordering = ['id']


class AsientoDebeDetalle(models.Model):
    asiento = models.ForeignKey(AsientoContable)
    cuenta = models.ForeignKey(SubCuenta)
    monto = models.DecimalField(max_digits=19, decimal_places=2)

    def __str__(self):
        return str(self.id) + " " + str(self.asiento)


class AsientoHaberDetalle(models.Model):
    asiento = models.ForeignKey(AsientoContable)
    cuenta = models.ForeignKey(SubCuenta)
    monto = models.DecimalField(max_digits=19, decimal_places=2)

    def __str__(self):
        return str(self.id) + " " + str(self.asiento)


## funciones de consulta a la base de datos
#def generar_balance(fecha_desde, fecha_hasta):
#    '''Genera el balance General'''
#    # TODO: Tema de las fechas, como hacer eso?
#    # tal vez se podria pasar el parametro de mes, luego cuando se traen los
#    # saldos de AsientoDebeDetalle y AsientoHaberDetalle filtrar por fecha,
#    # usando filter (date <= fecha_que_me_paso_el_usuario)
#    # aporte fantastico de fede.caceres
#    # http://docs.djangoproject.com/en/1.2/ref/models/querysets/#range
#
#    grupos_de_cuentas = TipoCuenta.objects.all()
#    diccionario_balance = {}
#    # Disculpame Kreitmayr por la falta de recursividad en esta parte :)
#    for g in grupos_de_cuentas:
#        diccionario_balance[g] = {'suma': 0}
#        lista_nivel1 = CuentaNivel1.objects.filter(tipo=g)
#        for n1 in lista_nivel1:
#            diccionario_balance[g][n1] = {'suma': 0}
#            lista_nivel2 = CuentaNivel2.objects.filter(tipo=n1)
#            for n2 in lista_nivel2:
#                diccionario_balance[g][n1][n2] = {'suma': 0}
#                lista_nivel3 = CuentaNivel3.objects.filter(tipo=n2)
#                for n3 in lista_nivel3:
#                    diccionario_balance[g][n1][n2][n3] = {'suma': 0}
#
#    # ya tenemos la lista completa de cuentas, ahora debemos calcular el saldo a
#    # la fecha del balance
#    # recorremos nuestro super diccionario de la manera mas ineficaz
#    # TODO: como consultar esto en forma mas eficaz
#    # segun fede.caceres, se podria usar esto
#    # http://docs.djangoproject.com/en/1.2/topics/db/aggregation/
#    for g in diccionario_balance:
#        if g != 'suma':
#            for n1 in diccionario_balance[g]:
#                if n1 != 'suma':
#                    for n2 in diccionario_balance[g][n1]:
#                        if n2 != 'suma':
#                            for n3 in diccionario_balance[g][n1][n2]:
#                                if n3 != 'suma':
#                                    saldo = 0
#                                    lista_monto_debe_n3 = AsientoDebeDetalle.objects.filter(cuenta=n3)
#                                    suma_monto_debe_n3 = 0
#                                    for i in lista_monto_debe_n3:
#                                        suma_monto_debe_n3 += i.monto
#                                    lista_monto_haber_n3 = AsientoHaberDetalle.objects.filter(cuenta=n3)
#                                    suma_monto_haber_n3 = 0
#                                    for i in lista_monto_haber_n3:
#                                        suma_monto_haber_n3 += i.monto
#                                    saldo = suma_monto_debe_n3 - suma_monto_haber_n3
#                                    if g.tipo_de_saldo == 'h':
#                                        saldo *= -1 # por el tema del saldo, debe y haber, segun Luca Paccioli
#                                    diccionario_balance[g][n1][n2][n3]['suma'] = saldo
#                                    diccionario_balance[g][n1][n2]['suma'] += saldo
#                            diccionario_balance[g][n1]['suma'] += diccionario_balance[g][n1][n2]['suma']
#                    diccionario_balance[g]['suma'] += diccionario_balance[g][n1]['suma']
#    # hasta aqui ya tenemos el balance
#    return diccionario_balance

