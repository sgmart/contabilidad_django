from django.db import models

# importa modelos definidos en cuentas_contables 
from cuentas_contables.models import SubCuenta, AsientoContable


class Poliza(models.Model):
    numero_poliza = models.CharField(max_length=20, unique=True)
    concepto = models.TextField('Concepto del pago')

    fecha = models.DateField()
    asiento = models.OneToOneField(AsientoContable)

    elaboro = models.CharField(max_length=65)
    reviso = models.CharField(max_length=65)
    autorizo = models.CharField(max_length=65)

    class Meta:
        abstract = True

    def __str__(self):
        return str(self.numero_poliza) + " " + self.asiento.comentario


class PolizaCheque(Poliza):
    beneficiario = models.ForeignKey(SubCuenta, related_name='ben_involucrado',
                            limit_choices_to={'clave__startswith': '120'})
    importe_numero = models.DecimalField(max_digits=19, decimal_places=2)
    importe_letra = models.CharField(max_length=250)
    banco = models.ForeignKey(SubCuenta, related_name='banco_involucrado',
                            limit_choices_to={'clave__startswith': '105'})
    folio_cheque = models.CharField(max_length=20, unique=True)


class PolizaIngreso(Poliza):
    class Meta:
        unique_together = ('numero_poliza', 'asiento')


class PolizaDiario(Poliza):
    class Meta:
        unique_together = ('numero_poliza', 'asiento')

