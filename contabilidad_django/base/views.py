from django.shortcuts import render_to_response
from django.template import Context
from django.http import HttpResponse, Http404, HttpResponseRedirect

from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.platypus import (SimpleDocTemplate, Paragraph, Image, Spacer,
        PageBreak, Table, TableStyle)

from polizas.models import PolizaCheque



def hello_pdf(request):
    response = HttpResponse(content_type='application/pdf')

    response['Content-Disposition'] = 'attachment; filename=hellow.pdf'

    p = canvas.Canvas(response)
    
    polizass = PolizaCheque.objects.all()
    i = 100
    g = 100
    data =  [['no_poliza', 'fecha', 'beneficiario', 'importe'],]
    t = Table(data)

    for poliza in polizass:
        p.drawString(i, g+90, str(poliza.importe_numero))
        p.drawString(i, g+100, poliza.importe_letra)
        g += 100

    p.showPage()
    p.save()

    return response

